/*
    Daniel Stroh

    12/10/2020

    This is a header file for a simple class to hold positions indicated by algebraic notation

    I created this because indexing into a checkerboard with this is completely safe. There is
        no possiblity to index out of bounds. If the indexes would be out of bounds, then you can't
        create an algebraicPosition from those indexes. It would throw an exception of some kind.

    This file has 6 *things*:
        1x enum
        1x exception class
        1x class constructor
        3x helper functions

    From top to bottom:

        algebraicColumn is an enum that represents the different columns on a checkerboard. I don't
            remember why I set it to start at 1, but I had a good reason for it.

        illegal_row is a class that inherits from the exeption class out_of_range. The constructor
            sets out_of_range's message to a simple error message.

        algebraicPosition() is the constructor for algebraicPositions. It throws illegal_row if the
            row is out of bounds. It also prints some debug info to cerr if the row is OOB

        to_num() is a helper function that returns a pair of numbers that is the numerical representation
            of algebraicPosition. The numbers range from 1-8.

        str() is a helper function that turns an algebraicPosition into a string representation.
            EX: turns {algebraicColumn::a, 4} into "a4"

        operator==() is a helper operator overload that implements == and !=. Very useful
*/

#pragma once

#include <stdexcept>
#include <string>

//An enum, so we limit the access to out of bounds areas
enum class algebraicColumn {
    a=1,
    b=2,
    c=3,
    d=4,
    e=5,
    f=6,
    g=7,
    h=8,
};

//! Custom Exception handling

//This is a simple exception class. Used to indicate that the row number is too large
// or too small when creating an algebraic position
class illegal_row : public std::out_of_range
{
public:
    illegal_row() : out_of_range("Only 1-8 is permitted") {};
};



// This class is a very simple class that contains an algebraic position on the board
// More info here : https://en.wikipedia.org/wiki/Algebraic_notation_(chess)
class algebraicPosition {
public:

    //This takes a column and a row starting at 1.
    algebraicPosition(algebraicColumn column, unsigned short row);


    //Convert the position to two numbers that can be used to index an array
    [[nodiscard]] std::pair<unsigned short, unsigned short> to_num() const;


    //A function to create a string representation
    [[nodiscard]] std::string str() const;


    //! Operator overloading

    //Useful comparison operator
    bool operator==(const algebraicPosition& rhs) const;

private:

    //This could be stored as an int, and probably should have been
    // but this takes less space
    unsigned short m_row;


    algebraicColumn m_column;
};
