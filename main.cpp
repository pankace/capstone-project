/*
    Daniel Stroh

    12/9/20

    This is the main file of the program.

    This file contains:

        1x Compile Time Flag
        1x Helper function
        1x Core functon

    From top to bottom:

        use_ai is a compile time flag. It indicates whether or not to use the ai, or
            have a person playing both sides.

        clear_terminal() is a helper function. It clears the terminal. Since I program
            mostly in linux, it is platform agnostic to a degree.

        main() is a Core function. This is the main REPL of the program. REPL stands for
            Read-Evaluate-Print-Loop. It reads the user input, evaluates the move, prints
            the board after the move has been made or the error, and loops back to the
            beginning
*/




#include <iostream>
#include <cstdlib>
#include <string>

#include "checkers.hpp"
#include "ai.hpp"

using namespace std;


//! Compile Time flags

const bool use_ai = true;


//Found this online. It's not the most OS Agnostic thing, but it works
// How I believe this works is by first trying to use the windows command
// "CLS" which is clear screen. If that returns with a non-zero return value
// it gets evaulated as true, and it trys "clear", the linux command to clear
// the terminal. It should work, fingers crossed
void clear_terminal()
{
    if (system("CLS") != 0) {
        system("clear");
    }
}

int main()
{
    checkersBoard b;


    string prompt = "Please enter two coordinates (q to quit): \n";
    string turn = "It is currently red's turn. \n";
    string error;

    //This is the main loop. Technically a REPL
    while (!b.win())
    {

        //! Output control

        //Clear the terminal to prepare for the next round
        clear_terminal();
        //Print the board and the prompt and any errors
        cout << b.str() << endl;



        //Dummy value that will get overriden later
        checkerMove m {{algebraicColumn::a, 1}, {algebraicColumn::a, 1}};

        if (b.current_turn() == pieceColor::red || !use_ai)
        {

            //! Error printing and user prompting

            cout << error << prompt << turn;

            //! User Input

            //Read the user input
            string user_input_source;
            cin >> user_input_source;


            //Add an exit command.
            if (user_input_source.at(0) == 'q') {
                break;
            }

            //Read more user input
            string user_input_destination;
            cin >> user_input_destination;


            //Extract the information out of the user input
            int sx = user_input_source.at(0) - 'a' +1;
            unsigned short sy = user_input_source.at(1) - '1' +1;

            int dx = user_input_destination.at(0) - 'a' +1;
            unsigned short dy = user_input_destination.at(1) -'1' +1;


            //Convert the user input to an algebraicPosition. This throws if the row is too large
            // But as it's a constructor, there's not a good way to catch the exception
            algebraicPosition src = {static_cast<algebraicColumn>(sx), sy};
            algebraicPosition dest = {static_cast<algebraicColumn>(dx), dy};


            //Put the source and destination into a checkerMove
            m = { src, dest };

        } else {
            cout << turn;
            cout << "Processing..." << endl;
            m = ai::find_best_move(b);
        }


        //Move the piece, and if it fails, set the error and retry
        if (!b.move(m))
        {
            error = "Invalid move. \n\n";
        } else {
            error = "";
        }

        //Create a turn string
        string temp = static_cast<bool>(b.current_turn()) ? "red's" : "black's";
        turn = "It is currently " + temp + " turn. \n";

    }


    //If there was a win condition,
    if (b.win())
    {
        //Decide the vitory and
        string victory = (b.winner() == pieceColor::red? "Congradulations red, you won!" : "Congradulations black, you Won!");

        //Clear the terminal and
        clear_terminal();

        //Declare the winner
        cout << b.str() << endl;
        cout << victory << endl;
    }


    return 0;
}
