/*
    Daniel Stroh

    12/09/2020

    This is going to be a massive comment.

    This is impl'ed by checkers.cpp. See that file for implementation details

    This file as 26 *things*:

        8x Compile time settings
        2x Enums
       15x Helper functions
        5x Core Functions and constructors

    From top to bottom:

        8x const string flags. These are the pieces grouped in an easy location to change.

        pieceColor and pieceType are two enums used to describe a checker piece. They describe
            the color, red or black, and the type, regular or king.

        checkerPiece::color() and ::type() are getter functions for checkerPiece. They just
            return the pieces color and type.

        checkerPiece::str() is a helper function. It maps a checkerPiece with a color and type to
            one of the 8 const strings to create a string representation of the pieces.

        checkerPiece() is a class constructor for checkerPiece. It takes a color and optionally a
            type and returns a checkerPiece().

        checkerMove() is a class constructor for checkerMove. It takes an algebraicPosition as a source
            and an algebraicPosition as the destination.

        checkerMove::validate() is a core function that checks if the move is a valid, allowable move. It
            checks things like if the piece being moved a) exists, and b) is the right color. This is the
            meat of the checkers engine.

        checkerMove::src() and ::dest() are getter functions for checkerMove. They return the source position
            and destination position respectively.

        checkerMove::operator==() is a helper operator overload. It implements == and !=, and is used to compare
            if too moves are equivalent.

        checkersBoard() is a class constructor for checkersBoard. It takes a flag on whether or not to fill the
            board with the standard layout.

        checkersBoard::str() is a helper function. It returns the string representation for the board.

        checkersBoard::win() and ::winner() are helper getter functions. They return if the board is in a won state
            and who the winner is. If the board is not in a win state, then the winner is black.

        checkersBoard::current_turn() is a helper getter function that returns whose turn it currently is.

        checkersBoard::move() is a Core Function that takes a move, checks if it's valid, then performs the move,
            by moving the pieces and removing jumped pieces

        checkersBoard::*_count() are helper getter functions that return the count of black kings, red kings, all
            black pieces, and all red pieces.
*/






#pragma once


#include <string>
//std::array lets me use range based for loops among other things. Very nice
#include <array>
//This is a relatively new feature, introduced in C++17. Not sure if it will work with VStudio
#include <optional>

#include "algebraicPosition.hpp"


//! Compile time flags

//Flag for using emoji to represent the pieces, or ASCII characters
//#define EMOJI_PIECES

#ifdef EMOJI_PIECES

    //I would love to make these chars or wchar_ts, but I haven't found a nice way
    // to store them in those. So constant string literals works just fine.
    const std::string RED_PIECE = "♙";
    const std::string BLACK_PIECE = "♟️";
    const std::string RED_KING = "♔";
    const std::string BLACK_KING = "♚";

#else

    //In case your terminal doesn't support unicode characters.
    const std::string RED_PIECE = "r";
    const std::string BLACK_PIECE = "b";
    const std::string RED_KING = "R";
    const std::string BLACK_KING = "B";

#endif


//These describe a checkers piece in it's entirety
//  They are enum class to prevent implicit conversion
enum class pieceColor {
    black,
    red,
};

enum class pieceType {
    regular,
    king,
};

//! Class / Object Oriented Structure

//A simple class to hold information about the piece
class checkerPiece
{
public:

    //Simple getter functions
    [[nodiscard]] pieceColor color() const { return m_color; };
    [[nodiscard]] pieceType type() const { return m_type; };

    //Converts the piece to a string for printing uses
    [[nodiscard]] std::string str() const;

    //This has to be explicit to prevent implicit conversion from pieceColor
    explicit checkerPiece(pieceColor c, pieceType t = pieceType::regular);

private:
    pieceColor m_color;
    pieceType m_type;
};

//! Dummy class for circular dependency

//This is because of a circular "dependency" where each class has a function that takes the other
// class as an argument so neither class can be fully declared before the other. It's dumb
class checkersBoard;



//This holds two positions, the source and the destination. Useful container
class checkerMove {
public:

    checkerMove(algebraicPosition from, algebraicPosition to);

    //This takes move and validates that it's correct.
    // This is the workhorse of the engine
    //Returns true if the move is valid, false if not
    [[nodiscard]] bool validate(checkersBoard& board) const;

    //Simple getter functions
    [[nodiscard]] algebraicPosition src() const { return m_src; };
    [[nodiscard]] algebraicPosition dest() const { return m_dest; };


    //! Operator overloading

    //Check if two moves are the same
    bool operator==(const checkerMove& rhs) const;

private:
    algebraicPosition m_src;
    algebraicPosition m_dest;
};



//A class to hold the board and functions that act upon it
class checkersBoard
{
public:

    //This is explicit to prevent auto converting from bools. standard_fill is a flag on whether or not to
    // fill the board with the standard checkerboard layout
    explicit checkersBoard(bool standard_fill = true);


    //! Operator overloading

    //Allow indexing with an algebraicPosition
    std::optional<checkerPiece>& operator[](algebraicPosition index);

    //Returns the board in a pretty state. Used for printing the board
    [[nodiscard]] std::string str() const;


    //Getter function. True if the game has been won, false if not
    [[nodiscard]] bool win() const { return m_win; };

    [[nodiscard]] pieceColor winner() const { return m_winner; };

    //Returns the color whose turn it is currently
    [[nodiscard]] pieceColor current_turn() const { return m_current_turn; };



    //This moves a piece from 'move.src()' to 'move.dest()'.
    bool move(checkerMove move);

    [[nodiscard]] int black_king_count() const { return m_black_king_count; };
    [[nodiscard]] int red_king_count() const { return m_red_king_count; };
    [[nodiscard]] int black_count() const { return m_black_count; };
    [[nodiscard]] int red_count() const { return m_red_count; };

private:
    //This checks row 1 and row 8 for pieces that need to be promoted
    // and promotes them
    void check_for_kings();

    //Checks to see if either side has no more pieces left
    void check_for_win();

    //Decreases the count of the color of pieces left
    void count_dec(pieceColor color, pieceType type);

    //Whose turn it currently is
    pieceColor m_current_turn;

    //m_win is initialized as false, and m_winner as red
    bool m_win;
    pieceColor m_winner;

    //Internal counters to prevent unnecessary iterating
    int m_red_count;
    int m_black_count;

    int m_red_king_count;
    int m_black_king_count;


    //! Smart types?

    //Found out about this. It apparently doesn't decay into a pointer when passed as an argument,
    // and is all around better. Apparently. It does require another header, and it's more verbose
    // but I'll try it out. No harm in trying.
    std::array<std::array<std::optional<checkerPiece>, 8>, 8> m_board;
};
