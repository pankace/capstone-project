/*
    Daniel Stroh

    12/10/2020

    This is the impl file for algebraicPosition.hpp. Check that file for a more detailed
     header comment
*/


#include "algebraicPosition.hpp"


#include <iostream>
#include <string>

using namespace std;


//This constructor will throw if the row is out of range. It will print the row
// to cerr, but should probably instead set the message of the exception
algebraicPosition::algebraicPosition(algebraicColumn column, unsigned short row)
{
    if (row > 8 || row < 1)
    {
        cerr << "Row=" << row << endl;

        //! Exception throwing

        throw illegal_row();
    }


    m_row = row;
    m_column = column;
}


//Decomposes an algebraicPosition to two numbers in column row order
//Columns range from 0 on the left to 7 on the right
// Rows range from 0 on top to 7 on bottom
pair<unsigned short, unsigned short> algebraicPosition::to_num() const
{
    int x = static_cast<int>(m_column) -1;
    return make_pair(x,8-m_row);
}


//Creates a string representation of the algebraicPosition
// Ex: if m_column == algebraicColumn::a and m_row == 3, then this returns a3
std::string algebraicPosition::str() const
{
        string one = string(1, ('a' + (static_cast<int>(m_column) -1)));
        string two = to_string(m_row);
        return one + two;
}


//! Operator Overloading

bool algebraicPosition::operator==(const algebraicPosition& rhs) const
//This is a wrapper to just pass this on to comparing the numbers inside
{
    return (to_num() == rhs.to_num());
}
