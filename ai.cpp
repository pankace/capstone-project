/*
    Daniel Stroh

    12/11/2020

    Credits to wikipedia for their wonderful description of the minimax algorithm


    This is the impl file for ai.hpp. Check that file for a more detailed header comment
*/





#include "ai.hpp"

using namespace std;


//Working in the ai namespace
namespace ai {



//Values of each action. Used to help make decisions
enum outcomes {
    lost_game = -999999,
    won_game = 999999,
};



//This function takes a board and returns every possible move the current player could take
vector<checkerMove> gen_moves(checkersBoard& board)
{

    //The final vector
    vector<checkerMove> results;

    //A vector of every piece position
    vector<algebraicPosition> piece_positions;
    bool odd = false;

    //Iterate over every row
    for ( auto i = 0; i < 8; i++ )
    {
        //Iterate over every other space
        for ( int j = static_cast<int>(odd); j < 8; j += 2)
        {

            //Convert the indexes to an algebraicPosition
            algebraicPosition src {static_cast<algebraicColumn>(i+ 1), static_cast<unsigned short>(8-j)};


            //If there is a piece on that square, and it's the current players piece
            if (board[src].has_value() && board[src].value().color() == board.current_turn())
            {
                //Add it to the vector of piece positions
                piece_positions.push_back(src);
            }
        }
        odd = !odd;
    }


    //This is a vector of all the open spaces
    vector<algebraicPosition> open_moves;
    odd = false;

    //Iterate every row
    for (auto i = 0; i < 8; i++)
    {
        //In every row, iterate over every other space
        for (int j = static_cast<int>(odd); j < 8; j += 2)
        {
            //Turn indexes into an algebraicPosition
            algebraicPosition src {static_cast<algebraicColumn>(i+ 1), static_cast<unsigned short>(8-j)};

            //Check if the space is open
            if(!board[src].has_value())
            {
                //Push onto the vector
                open_moves.push_back(src);
            }
        }
        odd = !odd;
    }


    //Iterate over every piece
    for (auto piece_position : piece_positions)
    {
        //Iterate over every open space
        for (auto open_space : open_moves)
        {
            //Create a move from the piece to the open space
            checkerMove move = {piece_position, open_space};

            //Check if the move is valid
            if (move.validate(board))
            {
                //If so, push it onto the results vector
                results.push_back(move);
            }
        }
    }

    //Return the final results
    return results;
}



//This finds the value of any one board for the board's current player
int value(checkersBoard& board)
{
    //If the board has been won, it has a very high value
    if (board.win())
    {
        //If the current player won, it has high value
        if (board.winner() == board.current_turn())
        {
            return outcomes::won_game;

        //Else it has a very low value
        } else {
            return outcomes::lost_game;
        }
    }

    int result = 0;

    if ( board.current_turn() == pieceColor::black)
    {
        //Here we are saying that the value of a king is worth 3 regular pieces
        // and the value of the board is how many more pieces we have
        result += (board.black_king_count() - board.red_king_count()) * 3;
        result += board.black_count() - board.red_count();
    } else {
        result += (board.red_king_count() - board.black_king_count()) * 3;
        result += board.red_count() - board.black_count();
    }

    return result;
}



//Here we turn a potential move into a copy of the board if that move is taken
checkersBoard look_ahead(checkersBoard& board, checkerMove move)
{
    auto result = checkersBoard(board);

    result.move(move);
    return result;
}



//Workhorse of the ai. This recursively calls itself trying to find the best possible move
// It takes a board, the depth remaining to travel, and if we are maximizing this turn or minimizing
int min_max(checkersBoard& board, int depth, bool maximizingPlayer)
{
    //If this is the last depth to search, or the board has been won. we return the value of the board
    //  This is the base case for the recursiveness coming up later
    if (depth == 0 || board.win())
    {

        return value(board);
    }

    //If we are maximizing the player, ie we want them to do well
    if (maximizingPlayer)
    {
        //we assume the best case is a lost game
        int result = outcomes::lost_game;

        //then for every move
        for (auto& move : gen_moves(board))
        {
            //We create a new board
            auto perhaps_board = look_ahead(board, move);


            //! Recursion

            //and take the max value of either the current highest or the next board
            //To find the value of this board, we run the min_max function on that
            // board with a lowered depth and we try to minimize the next players move
            result = max(result, min_max(perhaps_board, depth - 1, false));
        }

        //After that loop, this is the value of the most valuable move the ai can make.
        // We try to maximize this
        return result;
    } else {
    //Here we are trying to minimize the gains of the current player

        //We assume the best case is a won game
        int result = outcomes::won_game;

        //For every move possible
        for (auto& move : gen_moves(board))
        {
            //We create a board that took that move
            auto perhaps_board = look_ahead(board, move);


            //! Recursion

            //and find the minimum value move of either the worst so far,
            // or the worse of the next board by recursively calling min_max
            result = min(result, min_max(perhaps_board, depth - 1, true));
        }

        //and this is the best move that the non ai player can make. We try to minimize this
        return result;
    }
}



//This is the entry point of the ai. Takes a board, and returns the best possible move for it to make
checkerMove find_best_move(checkersBoard& board)
{

    //Assume worst case scenario. This will get overwritten
    int most_value = -999999999;

    //Dummy value that will get overwritten
    checkerMove m {{algebraicColumn::a, 1}, {algebraicColumn::a, 1}};

    //for every possible move
    for (auto move : gen_moves(board))
    {
        //Find the most valuable move possible
        int value = min_max(board, ai::DEPTH_TO_SEARCH, true);
        if ( value > most_value)
        {
            most_value = value;
            m = move;
        }
    }

    //And return that most valuable move.
    return m;
}
}
