/*
    Daniel Stroh

    12/09/2020

    This is the impl for checkers.hpp. Check that file for a more detailed
        header comment
*/


#include <iostream>
#include "checkers.hpp"

using namespace std;

/********** checkerPiece **********/

checkerPiece::checkerPiece(pieceColor c, pieceType t){
    m_color = c;
    m_type = t;
};

string checkerPiece::str() const
{
    if (type() == pieceType::regular && color() == pieceColor::black)
    {
        return BLACK_PIECE;
    }
    if (type() == pieceType::regular && color() == pieceColor::red) {
        return RED_PIECE;
    }
    if (type() ==pieceType::king && color() == pieceColor::black) {
        return BLACK_KING;
    }
    return RED_KING;
}

/********** checkerMove **********/

checkerMove::checkerMove(algebraicPosition from, algebraicPosition to) :
    m_src(from), m_dest(to)
{
}

bool checkerMove::validate(checkersBoard& board) const
{
    //Check that we aren't moving a piece to itself
    if (src() != dest())
    {
        //Check that we are moving a piece to an empty spot
        if (board[src()].has_value() && !board[dest()].has_value())
        {
            auto s = board[src()].value();

            //Check that we are moving the correct person's piece
            if (s.color() == board.current_turn())
            {
                //Complex moving rules.
                auto sx = get<0>(src().to_num());
                auto sy = get<1>(src().to_num());
                auto dx = get<0>(dest().to_num());
                auto dy = get<1>(dest().to_num());

                if (abs(sx-dx) == 1 && abs(sy-dy) == 1)
                {
                    //If we are here, we want to do a simple move

                    if (s.type() == pieceType::regular)
                    {
                        //Regular pieces can only move forward

                        if (s.color() == pieceColor::red)
                        {
                            if (sy > dy)
                            {
                                return true;
                            }
                        } else {
                            if (sy < dy)
                            {
                                return true;
                            }
                        }


                    } else {
                        //King movement.

                        return true;

                    }
                } else {
                    // Jumping time. We are capturing pieces here
                    if (abs(sx-dx) == 2 && abs(sy-dy) == 2)
                    {
                        //Single capture.
                        algebraicPosition m {static_cast<algebraicColumn>(((sx+dx)/2) + 1), static_cast<unsigned short>(8 - (sy+dy)/2)};
                        if (board[m].has_value() && board[m].value().color() != s.color())
                        {
                            //Capture by a regular piece
                            if (s.type() == pieceType::regular)
                            {
                                if (s.color() == pieceColor::red)
                                {
                                    //Red regular piece
                                    if (sy > dy)
                                    {
                                        return true;
                                    }
                                } else {
                                    //Black regular piece
                                    if (sy < dy)
                                    {
                                        return true;
                                    }
                                }
                            } else {
                                //Capture by a king
                                return true;
                            }
                        }
                    } else if (abs(sx-dx) == 4 && abs(sy-dy) == 4) {

                        //Double jump.

                        auto ax = (sx+dx)/2;
                        auto ay = (sy+dy)/2;
                        algebraicPosition ms {static_cast<algebraicColumn>(((ax+sx)/2) + 1), static_cast<unsigned short>((ay+sy)/2)};
                        algebraicPosition md {static_cast<algebraicColumn>(((ax+dx)/2) + 1), static_cast<unsigned short>(8-(ay+dy)/2)};
                        if (board[ms].has_value() && board[md].has_value() && board[ms].value().color() != s.color() && board[md].value().color() != s.color())
                        {
                            //Capture by a regular piece
                            if (s.type() == pieceType::regular)
                            {
                                if (s.color() == pieceColor::red)
                                {
                                    //Red regular piece
                                    if (sy > dy)
                                    {
                                        return true;
                                    }
                                } else {
                                    //Black regular piece
                                    if (sy < dy)
                                    {
                                        return true;
                                    }
                                }
                            } else {
                                //Capture by a king
                                return true;
                            }
                        }


                    } else if (abs(sx-dx) == 6 && abs(sy-dy) == 6) {

                    }
                }
            }
        }
    }
    return false;
}

//! Operator overloading

bool checkerMove::operator==(const checkerMove& rhs) const
{
    return (src() == rhs.src() && dest() == rhs.dest());
}

/********** checkersBoard **********/

//! Custom constructor

checkersBoard::checkersBoard(bool standard_fill)
{
    if (standard_fill) {
        array<array<optional<checkerPiece>, 8>, 8> board;

        //This could be cleaner and clearer
        board[0] = {checkerPiece(pieceColor::black), nullopt, checkerPiece(pieceColor::black), nullopt, checkerPiece(pieceColor::black), nullopt, checkerPiece(pieceColor::black), nullopt };
        board[1] = {nullopt, checkerPiece(pieceColor::black), nullopt, checkerPiece(pieceColor::black), nullopt, checkerPiece(pieceColor::black), nullopt, checkerPiece(pieceColor::black) };
        board[2] = {checkerPiece(pieceColor::black), nullopt, checkerPiece(pieceColor::black), nullopt, checkerPiece(pieceColor::black), nullopt, checkerPiece(pieceColor::black), nullopt };

        board[5] = {nullopt, checkerPiece(pieceColor::red), nullopt, checkerPiece(pieceColor::red), nullopt, checkerPiece(pieceColor::red), nullopt, checkerPiece(pieceColor::red) };
        board[6] = {checkerPiece(pieceColor::red), nullopt, checkerPiece(pieceColor::red), nullopt, checkerPiece(pieceColor::red), nullopt, checkerPiece(pieceColor::red), nullopt };
        board[7] = {nullopt, checkerPiece(pieceColor::red), nullopt, checkerPiece(pieceColor::red), nullopt, checkerPiece(pieceColor::red), nullopt, checkerPiece(pieceColor::red) };

        m_board = board;
    }
    m_current_turn = pieceColor::red;

    m_win = false;
    m_winner = pieceColor::red;

    m_red_count = 12;
    m_black_count = 12;
    m_red_king_count = 0;
    m_black_king_count = 0;
}


//! Operator overloading

optional<checkerPiece>& checkersBoard::operator[](algebraicPosition index)
{
    auto pos = index.to_num();
    unsigned short x = get<0>(pos);
    unsigned short y = get<1>(pos);
    return m_board[y][x];
}


string checkersBoard::str() const
{
    string result;
    string row1 = "|   |   |   |   |   |   |   |   |";
    string row2 = "|---+---+---+---+---+---+---+---|";
    string brow = "  a   b   c   d   e   f   g   h  ";


    result += row2;
    result += "\n";
    for (int y = 0; y < 8; y++)
    {
        string temp = row1;
        for (int x = 7; x >= 0; x--)
        {
            optional<checkerPiece> n = m_board[y][x];
            if (n.has_value())
            {
                //1, 5, 9, 13, 17, 21, 25, 29
                temp.replace((x*4)+2, 1, n.value().str());
            }
        }

        result += temp;
        result += " " + to_string(8-y);
        result += "\n";
        result += row2;
        result += "\n";
    }
    result += brow;
    result += "\n";

    return result;
}



bool checkersBoard::move(checkerMove move)
{

    //! *this pointer?

    if (move.validate(*this)) {
        auto src = move.src();
        auto dest = move.dest();

        operator[](dest) = operator[](src);

        {
            auto sx = get<0>(src.to_num());
            auto sy = get<1>(src.to_num());
            auto dx = get<0>(dest.to_num());
            auto dy = get<1>(dest.to_num());

            bool left = sx > dx;
            bool down = sy > dy;

            int rm_index_y = 8-sy;
            for (auto rm_index_x = sx+1; rm_index_x != dx+1; rm_index_x += (left? -1 : 1))
            {
                algebraicPosition rm {static_cast<algebraicColumn>(rm_index_x), static_cast<unsigned short>(rm_index_y)};
                auto& rm_piece = operator[](rm);

                if (rm_piece.has_value() && !(rm == src))
                {
                    count_dec(rm_piece.value().color(), rm_piece.value().type());
                }

                rm_piece = nullopt;

                rm_index_y += (down? 1 : -1);
            }
        }

        m_current_turn = static_cast<pieceColor>(!static_cast<bool>(m_current_turn));

        check_for_win();

        check_for_kings();

        return true;
    }
    return false;
}





void checkersBoard::check_for_kings()
{
    for (auto& opt_piece : m_board[0])
    {
        if (opt_piece.has_value())
        {
            if (opt_piece.value().color() == pieceColor::red)
            {
                m_red_king_count++;
                opt_piece = checkerPiece(pieceColor::red, pieceType::king);
            }
        }
    }

    for (auto& opt_piece : m_board[7])
    {
        if (opt_piece.has_value())
        {
            if (opt_piece.value().color() == pieceColor::black)
            {
                m_black_king_count++;
                opt_piece = checkerPiece(pieceColor::black, pieceType::king);
            }
        }
    }
}




void checkersBoard::check_for_win()
{
    bool red_win = m_black_count == 0;
    bool black_win = m_red_count == 0;
    if (black_win ^ red_win)
    {
        m_win = true;
        if (black_win)
        {
            m_winner = pieceColor::black;
        } else {
            m_winner = pieceColor::red;
        }
    }
}



void checkersBoard::count_dec(pieceColor color, pieceType type)
{
    if (color == pieceColor::red)
    {
        if (type == pieceType::king)
        {
            m_red_king_count--;
        }
        m_red_count--;
    } else {
        if (type == pieceType::king)
        {
            m_black_king_count--;
        }
        m_black_count--;
    }
}
