/*
    Daniel Stroh

    12/11/2020

    Impl file is at ai.cpp. Check that out for the inner details

    This file has 4 *things* in it.
        1x  Compile time setting.
        2x  functions that are just helpers
        1x  function that is actually useful to the outer world

    Starting at the top:

        DEPTH_TO_SEARCH is a compile time setting. Ranging from 1 to the maximum integer,
            this tells the ai how far into the future to search. Setting this higher can
            lead to a tougher ai, but it also increases the search space, and therefore
            the processing time, very very rapidly. Recommended is around 5-6.

        gen_moves() is a helper function. It takes a checkerboard, and finds every possible
            move that the current player could make, and returns that list as a vector. It
            has very little use outside of the ai, so there is no reason to call it really.

        min_max() is the main algorithm. It takes a board, the depth to search and whether
            or not to maximize the players score and returns the value of the most valuable
            move for the current player to make. It takes into account the other player, and
            assumes they are making the best possible moves, so it also tries to minimize the
            other players score. This is a recursive function that will continue to search into
            future DEPTH_TO_SEARCH moves. That recursion is what makes the ai take so long

        find_best_move() is the main entry point of the ai. This is what you want to call to
            calculate the best move for the current player. It takes a board, and returns a move
            that it believes to be the best. This is the most useful function for an outside to use.

*/




#pragma once

#include <vector>
#include "checkers.hpp"
#include "algebraicPosition.hpp"

//Throw it in a namespace since there is no reason for a class here, and separating functions is always nice
namespace ai{

    //This is how far into the future the ai should search
    // On my computer, setting this to 7 means each move at the beginning takes 30 seconds
    //  setting it to 10 lead to me quitting the program after an hour of processing.
    //
    // Doing some back-of-the-napkin calculations leads me to believe that the ai is
    //  O(10^N) at least, where N is DEPTH_TO_SEARCH

    //Setting this higher takes exponentially more time, but the ai becomes a better player
    const int DEPTH_TO_SEARCH = 5;


    //This function takes the board and generates every possible move that the current player could take
    //  This probably could be optimized more, but I don't have the brain power to do so
    std::vector<checkerMove> gen_moves(checkersBoard& board);


    //This is the workhorse of the ai. It takes a board, and either maximizes gains
    // or minimizes losses. The name comes from the fact that it maximizes it's score
    // while minimizing the opponents. The algorithm I found on wikipedia, but the implementation
    // is my own. Very likely some small bugs
    int min_max(checkersBoard& board, int depth, bool maximizingPlayer);


    //This is the entry point for the ai. Takes a board, and returns what it thinks to be the best move
    // the current player could take.
    checkerMove find_best_move(checkersBoard& board);

}
